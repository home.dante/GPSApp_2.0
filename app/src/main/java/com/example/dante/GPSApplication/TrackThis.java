package com.example.dante.GPSApplication;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.R.id.message;
import static android.R.id.toggle;

public class TrackThis extends AppCompatActivity {
    private LocationManager locMan;
    private LocationListener locList;
    public static final String TAG = "HANDLER";
    //private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    private String password = "1";
    private int count = 0, interval = 0, check = 0;
    private String RequestingNumber = "";
    EditText edit;
    ToggleButton trackthis;
    String confirmed = "";
    SmsManager smsManager = SmsManager.getDefault();
    String timeUnit = "";
    int timeCount = 0;
    int messageCount = 0;


    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    private static TrackThis inst;

    public static TrackThis instance() {
        return inst;
    }

    @Override
    public void onStart() {
        super.onStart();
        inst = this;
    }
 String coordString="";
double lat;
    double lng;


    private BroadcastReceiver mUnreadSmsBroadCastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String smsBody = intent.getStringExtra(SmsBroadcastReceiver.EXTRA_SMS_BODY);
            String smsAddress = intent.getStringExtra(SmsBroadcastReceiver.EXTRA_SMS_ADDRESS);
            //updateList(smsBody);
            Log.d(TAG, "received" + smsAddress);
            //toastMessage("message" + smsBody.toString()+ " number  :"+smsAddress);

            if (smsAddress.contains(confirmed) && smsAddress.contains(edit.getText())) {
                if (smsBody.contains("SMSTEXT" + password)) {
                    Log.d(TAG, "RECEIVED");
                    smsManager.sendTextMessage(smsAddress, null, "current coords", null, null);
                    toastMessage("sending current coords");
                    setCoordString();
                    smsManager.sendTextMessage(smsAddress, null, coordString, null, null);


                } else if (smsBody.contains("t0") && smsBody.contains(password)) {
                    toastMessage("sending multiple coords");
                    parseMsg(smsBody);
                    doToast();
                }
            }
            if (smsBody.contains("begin" + password) && smsAddress.contains(edit.getText())) {
                Log.d(TAG, "begin" + password + " number is" + edit.getText().toString());
                smsManager.sendTextMessage(smsAddress, null, "begin ok", null, null);
                confirmed = smsAddress;

                // doToast();
            } else if (smsBody.contains("begin") && smsAddress.contains(edit.getText())) {
                smsManager.sendTextMessage(smsAddress, null, "begin fail", null, null);

            }
            if (SmsBroadcastReceiver.PRE_ADDRESS.equals(smsAddress)) {
                // notify your unread message
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mUnreadSmsBroadCastReceiver, new IntentFilter(SmsBroadcastReceiver.ACTION_UNREAD_SMS));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mUnreadSmsBroadCastReceiver);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_track_this);
        edit = (EditText) findViewById(R.id.TrackingNumber);
        trackthis = (ToggleButton) findViewById(R.id.button2);
        locMan = (LocationManager) getSystemService(LOCATION_SERVICE);
        locList = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

            lat =Double.valueOf(location.getLatitude());
                lng= Double.valueOf(location.getLongitude());

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(intent);
            }
        };
        if(Build.VERSION.SDK_INT>=Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(new String[]{
                        Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET
                }, 10);
                return;
            } else {

            }
        }



        locMan.requestLocationUpdates("gps", 5000, 0, locList);
       // edit.setEnabled(false);
        trackthis.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // The toggle is enabled
                    check=1;
                    RequestingNumber= edit.getText().toString();
                    edit.setEnabled(false);

                   // registerReceiver(mUnreadSmsBroadCastReceiver, new IntentFilter(SmsBroadcastReceiver.ACTION_UNREAD_SMS));

                    // doToast();
                } else {
                    check=0;
                    RequestingNumber ="";
                    edit.setEnabled(true);
                    // The toggle is disabled
                    //unregisterReceiver(mUnreadSmsBroadCastReceiver);
                }
            }
        });


//                onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults){
//
//        }
    }

    public void setCount(int count){
        this.count = count;
    }
    public void setInterval(int interval){
        this.interval = interval*1000;
    }


    public void SetNumber(String number){
        this.RequestingNumber = number;
    }
    public String GetNumber (){
        return this.RequestingNumber;
    }



    public void doToast()
    {
        Log.d(TAG,"TIME COUNT IS: " +timeCount);
        Log.d(TAG,"message COUNT IS: " +messageCount);
        final Handler handler= new Handler();


        handler.postDelayed(new Runnable(){

            @Override
            public void run() {if (count<=messageCount && check ==1) {
                // TODO Auto-generated method stub

                handler.postDelayed(this, timeCount);
                Log.d(TAG, "DOING SOMETHING");
                toastMessage("count: "+count+" for number :"+RequestingNumber);
                setCoordString();
                smsManager.sendTextMessage(RequestingNumber, null, coordString, null, null);

                count++;
            }
            }

        }, 1000);
        count=0;

    }
    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }
    public void parseMsg(String s){



      //  String s = new String("t029h006123456");
        Matcher matcher = Pattern.compile("t\\d\\d\\d").matcher(s);
        Matcher matcher2 = Pattern.compile("(h|m|s)\\d\\d\\d").matcher(s);
        matcher.find();
        matcher2.find();
        //int i = Integer.valueOf(matcher.group());
        String random= String.valueOf(matcher.group());
        String random2= String.valueOf(matcher2.group());
        //System.out.println("interval time is " +Integer.valueOf(random.substring(1))+" "+ random2.substring(0,1) +" units \n");
        //System.out.println(random.substring(1)+"\n");

        //System.out.println("number of messages is "+Integer.valueOf(random2.substring(1)));
        timeUnit= random2.substring(0,1);
        switch(timeUnit) {
            case "s":
                timeCount= 1000*Integer.valueOf(random.substring(1));;
                break;
            case "m":
                timeCount= 60*1000*Integer.valueOf(random.substring(1));;
                break;
            case "h":
                timeCount= 60*60*1000*Integer.valueOf(random.substring(1));
            default:
                timeCount= 1000*Integer.valueOf(random.substring(1));
        }


        messageCount = Integer.valueOf(random2.substring(1));




    }

    public void setCoordString() {
        coordString="lat:"+lat+" "+"long:"+lng+" "+"speed:0";

    }
}
