package com.example.dante.GPSApplication;


import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class SmsCatch extends AppCompatActivity {
    public static final String TAG="smscatch ";
    private BroadcastReceiver mUnreadSmsBroadCastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            String smsBody = intent.getStringExtra(SmsBroadcastReceiver.EXTRA_SMS_BODY);
            String smsAddress = intent.getStringExtra(SmsBroadcastReceiver.EXTRA_SMS_ADDRESS);
            //updateList(smsBody);
            Log.d(TAG, "received" +smsAddress);
            if (smsBody.contains("lat:")&& smsBody.contains("long:")){


            updateInbox(smsBody);
            }
            if (SmsBroadcastReceiver.PRE_ADDRESS.equals(smsAddress)) {
                // notify your unread message
            }
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver(mUnreadSmsBroadCastReceiver, new IntentFilter(SmsBroadcastReceiver.ACTION_UNREAD_SMS));
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mUnreadSmsBroadCastReceiver);
    }

        ArrayList<String> smsMessagesList = new ArrayList<>();
        ListView messages;
        ArrayAdapter arrayAdapter;

        SmsManager smsManager = SmsManager.getDefault();
        private static SmsCatch inst;

        private static final int READ_SMS_PERMISSIONS_REQUEST = 1;

        public static SmsCatch instance() {
            return inst;
        }

        @Override
        public void onStart() {
            super.onStart();
            inst = this;
        }


        @Override
        protected void onCreate(Bundle savedInstanceState) {




            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_sms_catch);

            messages = (ListView) findViewById(R.id.messages);

            arrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, smsMessagesList);
            messages.setAdapter(arrayAdapter);
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {
                getPermissionToReadSMS();
            } else {
                refreshSmsInbox();
            }


        }
//    Intent intent = getIntent();

    //Method for parsing out the coords

//    public double[] parsecoords (String message){
//
//       double params[] = new double[2];
//
//        String  messagelat = new String(message);
//        String messagelong = new String(message);
//
//
//        messagelat = messagelat.substring(messagelat.indexOf(":") + 1);
//        messagelat = messagelat.substring(0, messagelat.indexOf(" "));
//        params[0] = Double.valueOf(messagelat);
//
//
//        messagelong = messagelong.substring(messagelong.indexOf(':', messagelong.indexOf(':') + 1));
//        messagelong = messagelong.substring(0, messagelong.indexOf(" "));
//        params[1] = Double.valueOf(messagelong.substring(1));
//
//
//    return params;
//
//    }





    //Method for checking if the sms contains right number
//    private boolean isContainExactWord(String fullString, String partWord){
//        String pattern = "\\b"+partWord+"\\b";
//        Pattern p=Pattern.compile(pattern);
//        Matcher m=p.matcher(fullString);
//        return m.find();
//    }





//potential place to send the message
    public void updateInbox(final String smsMessage) {
        if (smsMessage.contains("lat:") && smsMessage.contains("long:")) {
            // double placeholder[] = new double[2];

//            if(isContainExactWord(smsMessage, "111")){
            arrayAdapter.insert(smsMessage, 0);
            arrayAdapter.notifyDataSetChanged();
            // Intent intent = new Intent(this, MapsActivity.class);
            //transform sms message into double array

//        placeholder[] = parsecoords(smsMessage);
//        intent.putExtra("var1",Double.parseDouble(String.valueOf(parsecoords(smsMessage))) );
//        intent.putExtra("var2",Double.parseDouble(String.valueOf(textlong.getText())) );
        }
    }



        public void getPermissionToReadSMS() {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                        != PackageManager.PERMISSION_GRANTED) {

                    if (shouldShowRequestPermissionRationale(
                            Manifest.permission.READ_SMS)) {
                        Toast.makeText(this, "Please allow permission!", Toast.LENGTH_SHORT).show();
                    }

                    requestPermissions(new String[]{Manifest.permission.READ_SMS},
                            READ_SMS_PERMISSIONS_REQUEST);
                }
            }
        }

        @Override
        public void onRequestPermissionsResult(int requestCode,
                                               @NonNull String permissions[],
                                               @NonNull int[] grantResults) {
            // Make sure it's our original READ_CONTACTS request
            if (requestCode == READ_SMS_PERMISSIONS_REQUEST) {
                if (grantResults.length == 1 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Read SMS permission granted", Toast.LENGTH_SHORT).show();
                    refreshSmsInbox();
                } else {
                    Toast.makeText(this, "Read SMS permission denied", Toast.LENGTH_SHORT).show();
                }

            } else {
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            }



        }

        public void refreshSmsInbox() {
            ContentResolver contentResolver = getContentResolver();
            Cursor smsInboxCursor = contentResolver.query(Uri.parse("content://sms/inbox"), null, null, null, null);
            int indexBody = smsInboxCursor.getColumnIndex("body");
            int indexAddress = smsInboxCursor.getColumnIndex("address");

            if (indexBody < 0 || !smsInboxCursor.moveToFirst()) return;
            arrayAdapter.clear();


                do {
                        if(smsInboxCursor.getString(indexBody).contains("lat:") && smsInboxCursor.getString(indexBody).contains("long:")) {
                            String str = "SMS From: " + smsInboxCursor.getString(indexAddress) +
                                    "\n" + smsInboxCursor.getString(indexBody) + "\n";
                            arrayAdapter.add(str);
                        }

                } while (smsInboxCursor.moveToNext());


        }



}
