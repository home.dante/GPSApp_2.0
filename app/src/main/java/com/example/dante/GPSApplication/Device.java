package com.example.dante.GPSApplication;

public class Device {

    private int _id;

    private String _devicename;

    private String _devicenumber;

    private String _devicepassword;

    //last coordonates recorded
    private double _longcoord;

    private double _latcoord;
    //was the password confirmed on the device?
    private int _confirmed;
    //last recored date/time this device was requested coordonates
    private String _lastchecked;
    //is "tracking" checked for this device?
    private int _tracking;

    public Device(){
    }

    public Device(String devicename, String devicepassword, String devicenumber,int tracking){
        this._devicename = devicename;
        this._devicepassword = devicepassword;
        this._devicenumber = devicenumber;
        this._tracking = tracking;

    }

    //set
    public void set_id(int _id) {
        this._id = _id;
    }

    public void set_devicename(String _devicename) {
        this._devicename = _devicename;
    }
    public void set_devicenumber(String _devicenumber) {
        this._devicenumber = _devicenumber;
    }
    public void set_devicepassword(String _devicepassword) {
        this._devicepassword = _devicepassword;
    }
    public void set_longcoord(double _longcoord) {
        this._longcoord = _longcoord;
    }
    public void set_latcoord(double _latcoord) {
        this._latcoord = _latcoord;
    }
    public void set_confirmed(int _confirmed) {
        this._confirmed = _confirmed;
    }
    public void set_lastchecked(String _lastchecked) {
        this._lastchecked = _lastchecked;
    }
    public void set_tracking(int _tracking){
        this._tracking = _tracking;
    }
    //set







    //get
    public int get_id() {
        return _id;
    }

    public String get_devicename() {
        return _devicename;
    }
    public String get_devicenumber(){
        return _devicenumber;
    }
    public String get_devicepassword(){
        return _devicepassword;
    }
    public String get_lastchecked(){
        return _lastchecked;
    }
    public  double get_longcoord(){
        return _longcoord;
    }
    public double get_latcoord(){
        return _latcoord;
    }
    public int get_confirmed(){
        return _confirmed;
    }
    public int get_tracking(){
        return _tracking;
    }
    //get
}