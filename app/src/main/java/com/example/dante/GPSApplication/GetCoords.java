package com.example.dante.GPSApplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import static com.example.dante.GPSApplication.R.id.radioGroup;


public class GetCoords extends AppCompatActivity {
     public static final String TAG= "GET COORDS ACTIVITY: " ;
    SmsManager smsManager = SmsManager.getDefault();
    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    EditText Number, Interval;
    Button GetCoords;
    MyDBHandler newdbhandler;
    String number, password;
    String timeunit;
    int IntNumber=0, IntInterval=0;
    String regex = "\\d+";
    String selected;
    RadioGroup Rgroup;
    String IntervalString;

   static public Boolean checked=false, disabled=false;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_coords);
      Number =(EditText) findViewById(R.id. MessageNumber);
       Interval=(EditText) findViewById(R.id. intervalTime);
        GetCoords =(Button) findViewById(R.id. btnGetCoords);

        newdbhandler= new MyDBHandler(this, null, null, 1);

        createRadioButtons();



    }

    private void createRadioButtons(){
        RadioGroup Rgroup = (RadioGroup) findViewById(R.id. radioGroup);
        String[] timevalues = getResources().getStringArray(R.array.time_values);
        for(int i =0; i<timevalues.length; i++){

            final String timevalue = timevalues[i];

            RadioButton button = new RadioButton(this);

           // button.setId("@+id/timevalue");

//            int k=button.getId();
//            Log.d(TAG,"ID OF THE BUTTON IS: "+String.valueOf(k));
            button.setText(timevalue);
//            if(timevalue.equals("Send once")){
//                button.setChecked(true);
//                Number.setText("");
//                Interval.setText("");
//                Number.setEnabled(false);
//                Interval.setEnabled(false);
//            }

            Rgroup.addView(button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(GetCoords.this,"clicked"+timevalue.toString(),Toast.LENGTH_SHORT).show();
                    if(timevalue.equals("Send once")){
                        Log.d(TAG,"SEND ONCE");
                        checked=true;
                        Number.setText("");
                        Interval.setText("");
                        Number.setEnabled(false);
                        Interval.setEnabled(false);
                        disabled=true;
                    }
                    else if(timevalue.equals("minutes")){
                        checked=true;
                        Log.d(TAG,"MINUTES");
                        Number.setEnabled(true);
                        Interval.setEnabled(true);
                        timeunit="m";
                    }
                    else if(timevalue.equals("seconds")){
                        checked=true;
                        Log.d(TAG,"SECONDS");
                        Number.setEnabled(true);
                        Interval.setEnabled(true);
                        timeunit="s";
                    }
                    else if(timevalue.equals("hours")){
                        checked=true;
                        Log.d(TAG,"HOURS");
                        Number.setEnabled(true);
                        Interval.setEnabled(true);
                        timeunit="h";
                    }
                }
            });
        }


    }
//
//    public void onRadioButtonClicked(View view) {
//        // Is the button now checked?
//        boolean checked = ((RadioButton) view).isChecked();
//
//        // Check which radio button was clicked
//        switch(view.getId()) {
//            case R.id.disabled:
//                if (checked)
//                    // Pirates are the best
//                Log.d(TAG, "secondsradio");
//                    break;
//            case R.id.:
//                if (checked)
//                    // Ninjas rule
//                    Log.d(TAG, "minutesradio");
//                    break;
//            case R.id.hoursradio:
//                if(checked)
//
//                    Log.d(TAG,"hoursradio");
//                break;
//            case R.id.disable:
//                if(checked)
//                    Log.d(TAG, "Disabled");
//                break;
//        }
//    }



    public void GetCoordsOnClick(View view) {
       IntNumber+= Integer.parseInt( 0+String.valueOf(Number.getText()));
       IntInterval+=Integer.parseInt(0+String.valueOf(Interval.getText()) );
        Log.d(TAG," INTNUMBER : "+ String.valueOf(Number.getText())+0 );
        Log.d(TAG," INTInterval : "+ String.valueOf(Interval.getText())+0 );
            if(checked) {
                Log.d(TAG,"before if");
                    if (IntNumber<=255 && IntInterval<=255 ) {
                        Log.d(TAG,"after if");
                        Log.d(TAG, " Button pressed");
                        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                                != PackageManager.PERMISSION_GRANTED) {
                            getPermissionToReadSMS();
                        } else {

                            //SetSmsString(timeunit, String.valueOf(Number.getText()), String.valueOf(Interval.getText()));
                            Log.d(TAG, " Permissions gathered");

                            //smsManager.sendTextMessage(edit_number.getText().toString(), null, "SMSTEXT"+edit_password.getText().toString(), null, null);
                            //Toast.makeText(this, "Message sent!", Toast.LENGTH_SHORT).show();

                            Cursor c = newdbhandler.MessagingCursor();
                            c.moveToFirst();
                            Log.d(TAG, " Cursor received");

                            //Position after the last row means the end of the results
                            while (!c.isAfterLast()) {
                                Log.d(TAG, " Loop started");
                                if (c.getString(c.getColumnIndex("_devicenumber")) != null && c.getString(c.getColumnIndex("_devicename")) != null) {
                                    Log.d(TAG, " inside if");
                                    number = c.getString(c.getColumnIndex("_devicenumber"));
                                    password = c.getString(c.getColumnIndex("_devicepassword"));
                                    smsManager.sendTextMessage(number, null, SetSmsString(timeunit, String.valueOf(Number.getText()), String.valueOf(Interval.getText())) + password, null, null);
                                    Log.d(TAG,"MESSAGE IS: "+SetSmsString(timeunit, String.valueOf(Number.getText()), String.valueOf(Interval.getText()))+password);
                                    Toast.makeText(this, "sms sent to " + number + ", message " + password, Toast.LENGTH_SHORT).show();
                                    Log.d(TAG, "sms sent to number +" + number + " message :" + password);

                                }
//

                                c.moveToNext();
                            }
                            Log.d(TAG, " Loop ended");
                            //db.close();
                        }
                    }
                else{
                        Toast.makeText(this,"Enter value between 0 and 255, at least 20 seconds", Toast.LENGTH_SHORT).show();
                        }
            }

        else {

                Toast.makeText(this,"Please check an option", Toast.LENGTH_SHORT).show();

        }


    }

    public void getPermissionToReadSMS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_SMS)) {
                    Toast.makeText(this, "Please allow permission!", Toast.LENGTH_SHORT).show();
                }

                requestPermissions(new String[]{Manifest.permission.READ_SMS},
                        READ_SMS_PERMISSIONS_REQUEST);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_SMS_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Read SMS permission granted", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "Read SMS permission denied", Toast.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }



    }

        static  String SetSmsString(String timeUnit, String number, String timeUnitCount){
            String smsString="";
            String zeros="000";

            if(disabled){
                return "SMSTEXT";
            }

            smsString +="t"+zeros.substring(zeros.length()-(3-timeUnitCount.length())) + timeUnitCount+timeUnit+zeros.substring(zeros.length()-(3-number.length())) +number;


            return smsString;
        }

}
