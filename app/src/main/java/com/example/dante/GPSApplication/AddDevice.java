package com.example.dante.GPSApplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import static android.R.id.input;

public class AddDevice extends AppCompatActivity {
    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    EditText nameinput;
    EditText passwordinput;
    EditText numberinput;
    TextView dbstringcontainer;
    MyDBHandler dbHandler;
    CheckBox tracking, status;
    SmsManager smsManager = SmsManager.getDefault();
  private static final String TAG = "ADDDEV" ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        nameinput = (EditText) findViewById(R.id.editname);
        passwordinput = (EditText) findViewById(R.id.editpassword);
        numberinput = (EditText) findViewById(R.id.editnumber);
        dbstringcontainer = (TextView) findViewById(R.id.dbstringcontainer);
        tracking = (CheckBox) findViewById(R.id.checkTracking);
        status =(CheckBox) findViewById(R.id.confirmstatus) ;

        dbHandler = new MyDBHandler(this, null, null, 1);
        clearAll();
        //printDatabase();
    }
    Intent devintent = getIntent();


    //add device
    public void addButtonClicked(View view){
        int val =0;

        if (tracking.isChecked() ){
            val = 1;

        }
        if (!dbHandler.CheckForExistance(numberinput.getText().toString())){
        Log.d(TAG,"add device name, pass, number, ischecked : "+ nameinput.getText()+ " " +passwordinput.getText()+" " + numberinput.getText()+" "+val);
        Device device = new Device(nameinput.getText().toString(), passwordinput.getText().toString(), numberinput.getText().toString() ,val );
       dbHandler.addDevice(device);}
        else{
            Toast.makeText(this, "Device already exists!", Toast.LENGTH_SHORT).show();
        }
        //TODO SEND MESSAGE HERE
//        SmsManager smsManager = SmsManager.getDefault();
//        smsManager.sendTextMessage(numberinput.getText().toString(), null, "password"+passwordinput.getText().toString(), null, null);
//        printDatabase();
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            getPermissionToReadSMS();
        } else {
            smsManager.sendTextMessage(numberinput.getText().toString(), null, "begin"+passwordinput.getText().toString(), null, null);
            Toast.makeText(this, "Message sent!", Toast.LENGTH_SHORT).show();
            clearAll();
        }


    }
    public void getPermissionToReadSMS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_SMS)) {
                    Toast.makeText(this, "Please allow permission!", Toast.LENGTH_SHORT).show();
                }

                requestPermissions(new String[]{Manifest.permission.READ_SMS},
                        READ_SMS_PERMISSIONS_REQUEST);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_SMS_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Read SMS permission granted", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "Read SMS permission denied", Toast.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }



    }

    public void clearAll(){
       // String dbString = dbHandler.databaseToString();
        //dbstringcontainer.setText(dbString);
        numberinput.setText("");
        passwordinput.setText("");
        nameinput.setText("");
        tracking.setChecked(false);
        status.setChecked(false);

    }

}
