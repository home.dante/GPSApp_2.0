package com.example.dante.GPSApplication;




import android.app.Application;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;
import android.util.Log;

public class MyDBHandler extends SQLiteOpenHelper {
    private static final String TAG = "MyDBHandler";
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "deviceDB.db";
    public static final String TABLE_DEVICES = "devices";
    public static final String TABLE_AUTHORIZED = "authorized";
    public static final String COLUMN_NUMBER = "_number";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_DEVICENAME = "_devicename";
    public static final String COLUMN_DEVICENUMBER = "_devicenumber";
    public static final String COLUMN_DEVICEPASSWORD = "_devicepassword";
    public static final String COLUMN_LONGCOORD = "_longcoord";
    public static final String COLUMN_LATCOORD = "_latcoord";
    public static final String COLUMN_CONFIRMED = "_confirmed";
    public static final String COLUMN_LASTCHECKED = "_lastchecked";
    public static final String COLUMN_TRACKING = "_tracking";



    //We need to pass database information along to superclass
    public MyDBHandler(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, DATABASE_NAME, factory, DATABASE_VERSION);
//
    }

    private Context context;

public void cleanup(){
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTHORIZED);
    String query2 = "CREATE TABLE " + TABLE_AUTHORIZED + "(" +
            COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            COLUMN_NUMBER + " TEXT" +

            ");";
    db.execSQL(query2);

}

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE " + TABLE_DEVICES + "(" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_DEVICENAME + " TEXT, " + COLUMN_DEVICENUMBER +
                " TEXT, " + COLUMN_DEVICEPASSWORD + " TEXT, " + COLUMN_LONGCOORD
                + " DOUBLE, " + COLUMN_LATCOORD + " DOUBLE, " + COLUMN_CONFIRMED +
                " INTEGER, " + COLUMN_LASTCHECKED + " TEXT, " + COLUMN_TRACKING + " INTEGER" +

                ");";
        db.execSQL(query);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_DEVICES);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUTHORIZED);
        onCreate(db);
    }

    public void addAuthorized(String number){
        ContentValues values2 = new ContentValues();
        values2.put(COLUMN_NUMBER, number);
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_AUTHORIZED, null, values2);
        db.close();
    }
    public Boolean CheckIfAuthorized(String addr){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_NUMBER + " FROM " + TABLE_AUTHORIZED +
                " WHERE " + COLUMN_NUMBER + " = '" + addr + "'";
        Cursor data3 = db.rawQuery(query, null);
        if (!(data3.moveToFirst()) || data3.getCount() ==0){
            //cursor is empty
            return false;
        }
        else return true;
    }

    //Add a new row to the database
    public void addDevice(Device device){
        ContentValues values = new ContentValues();
        values.put(COLUMN_DEVICENAME, device.get_devicename());
        values.put(COLUMN_DEVICENUMBER, device.get_devicenumber());
        values.put(COLUMN_DEVICEPASSWORD, device.get_devicepassword());
       // values.put(COLUMN_LONGCOORD, device.get_longcoord());
        //values.put(COLUMN_LATCOORD, device.get_latcoord());
       values.put(COLUMN_CONFIRMED, 0);
        values.put(COLUMN_TRACKING, device.get_tracking());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_DEVICES, null, values);
        db.close();
    }

    //Delete a product from the database
    public void deleteDevice(String deviceName){
        SQLiteDatabase db = getWritableDatabase();
        db.execSQL("DELETE FROM " + TABLE_DEVICES + " WHERE " + COLUMN_DEVICENAME + "=\"" + deviceName + "\";");
    }

    public String databaseToString(){
        String dbString = "";
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_DEVICES + " WHERE 1";

        //Cursor points to a location in your results
        Cursor c = db.rawQuery(query, null);
        //Move to the first row in your results
        c.moveToFirst();

        //Position after the last row means the end of the results
        while (!c.isAfterLast()) {
            if (c.getString(c.getColumnIndex("_devicename")) != null) {
                dbString += c.getString(c.getColumnIndex("_devicename"));
                dbString += "\n";

                dbString += c.getString(c.getColumnIndex("_devicenumber"));
             dbString += "\n";
                dbString += c.getString(c.getColumnIndex("_devicepassword"));
               dbString += "\n";
                dbString += c.getString(c.getColumnIndex("_confirmed"));
                dbString += "\n";

            }
//

            c.moveToNext();
        }
        db.close();
        return dbString;
    }
    //new
    public boolean addData(String item) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put(COLUMN_ID, item);

        Log.d(TAG, "addData: Adding " + item + " to " + TABLE_DEVICES);

        long result = db.insert(TABLE_DEVICES, null, contentValues);

        if (result == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Returns all the data from database
     * @return
     */
    public Cursor getData(){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_DEVICES;
        Cursor data = db.rawQuery(query, null);
        return data;
    }



    public Cursor getItemID(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_ID + ", "+ COLUMN_DEVICENAME+
                ", " +COLUMN_DEVICENUMBER+ ", " + COLUMN_DEVICEPASSWORD +
                ", "+ COLUMN_LATCOORD +", "+ COLUMN_LONGCOORD + ", "
                + COLUMN_CONFIRMED+", " + COLUMN_TRACKING+
                " FROM " + TABLE_DEVICES +
                " WHERE " + COLUMN_DEVICENAME + " = '" + name + "'";
        Cursor data = db.rawQuery(query, null);
        return data;
    }


    public Boolean CheckForExistance(String addr){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_DEVICENUMBER + " FROM " + TABLE_DEVICES +
                " WHERE " + COLUMN_DEVICENUMBER + " = '" + addr + "'";
        Cursor data3 = db.rawQuery(query, null);
        if (!(data3.moveToFirst()) || data3.getCount() ==0){
            //cursor is empty
            return false;
        }
        else return true;
    }

    public Boolean CheckForName(String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_DEVICENUMBER + " FROM " + TABLE_DEVICES +
                " WHERE " + COLUMN_DEVICENAME + " = '" + name + "'";
        Cursor data3 = db.rawQuery(query, null);
        if (!(data3.moveToFirst()) || data3.getCount() ==0){
            //cursor is empty
            return false;
        }
        else return true;
    }



    public void updateName(String newName, int id, String oldName){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_DEVICES + " SET " + COLUMN_DEVICENAME +
                " = '" + newName + "' WHERE " + COLUMN_ID + " = '" + id + "'" +
                " AND " + COLUMN_DEVICENAME + " = '" + oldName + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newName);
        db.execSQL(query);
    }
    public void updateNumber(String newNumber, int id, String oldNumber){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_DEVICES + " SET " + COLUMN_DEVICENUMBER +
                " = '" + newNumber + "' WHERE " + COLUMN_ID + " = '" + id + "'" +
                " AND " + COLUMN_DEVICENUMBER + " = '" + oldNumber + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newNumber);
        db.execSQL(query);
    }
    public void updatePassword(String newPassword, int id, String oldPassword){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_DEVICES + " SET " + COLUMN_DEVICEPASSWORD +
                " = '" + newPassword + "' WHERE " + COLUMN_ID + " = '" + id + "'" +
                " AND " + COLUMN_DEVICEPASSWORD + " = '" + oldPassword + "'";
        Log.d(TAG, "updateName: query: " + query);
        Log.d(TAG, "updateName: Setting name to " + newPassword);
        db.execSQL(query);
    }


    public void deleteName(int id, String name){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "DELETE FROM " + TABLE_DEVICES + " WHERE "
                + COLUMN_ID + " = '" + id + "'" +
                " AND " + COLUMN_DEVICENAME + " = '" + name + "'";
        Log.d(TAG, "deleteName: query: " + query);
        Log.d(TAG, "deleteName: Deleting " + name + " from database.");
        db.execSQL(query);
    }

    public Cursor isTracking(String addr){


           Log.d("TAG", "here we go");
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_ID + ", "+ COLUMN_DEVICENAME+
                ", " +COLUMN_DEVICENUMBER+ ", " + COLUMN_DEVICEPASSWORD +
                ", "+ COLUMN_LATCOORD +", "+ COLUMN_LONGCOORD + ", "
                + COLUMN_CONFIRMED+", " + COLUMN_TRACKING+
                " FROM " + TABLE_DEVICES +
                " WHERE " + COLUMN_DEVICENUMBER + " = '" + addr + "'";
        Log.d(TAG,"query is :"+query);
        Cursor data = db.rawQuery(query, null);
        data.moveToFirst();
        return data;



    }

    public void SetTracking(String number, int val){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_DEVICES + " SET " + COLUMN_TRACKING+
                " = '" + val + "' WHERE " + COLUMN_DEVICENUMBER + " = '" +  number + "'" ;
        Log.d(TAG, "update Status: " + query);
        Log.d(TAG, "update Status: Setting status to " + val);
        db.execSQL(query);
    }


    public int IsConfirmed(String addr){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_CONFIRMED + " FROM " + TABLE_DEVICES +
                " WHERE " + COLUMN_DEVICENUMBER + " = '" + addr + "'";
        Cursor data3 = db.rawQuery(query, null);

            return data3.getInt(0);

    }
    public void ConfirmDevice(String addr){

        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_DEVICES + " SET " + COLUMN_CONFIRMED +
                " = '" + 1 + "' WHERE " + COLUMN_DEVICENUMBER + " = '" +  addr + "'" ;
        Log.d(TAG, "update Status: " + query);
        Log.d(TAG, "update Status: Setting confirmation status to " + "TRUE");
        db.execSQL(query);

    }
    public void SetLong(String number, double lng){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_DEVICES + " SET " + COLUMN_LONGCOORD +
                " = '" + lng + "' WHERE " + COLUMN_DEVICENUMBER + " = '" +  number + "'" ;
        Log.d(TAG, "update Status: " + query);
        Log.d(TAG, "update Status: Setting status to " + "confirmed");
        db.execSQL(query);
        //notifyChange(null);
       // MapsActivity.reboot();
    }

    public void SetLat(String number, double lat){
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "UPDATE " + TABLE_DEVICES + " SET " + COLUMN_LATCOORD +
                " = '" + lat + "' WHERE " + COLUMN_DEVICENUMBER + " = '" +  number + "'" ;
        Log.d(TAG, "update Status: " + query);
        Log.d(TAG, "update Status: Setting status to " + "confirmed");
        db.execSQL(query);
      //  notifyChange(null);
    }

    public double GetLong(String name){
        double Long=0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_LONGCOORD + " FROM " + TABLE_DEVICES +
                " WHERE " + COLUMN_DEVICENAME + " = '" + name + "'";
        Cursor data3 = db.rawQuery(query, null);
        data3.moveToFirst();
        Long=data3.getDouble(data3.getColumnIndex("_longcoord"));
        return Long;

    }
    public Double GetLat(String name){

        double Lat=0;
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT " + COLUMN_LATCOORD + " FROM " + TABLE_DEVICES +
                " WHERE " + COLUMN_DEVICENAME + " = '" + name + "'";
        Cursor data3 = db.rawQuery(query, null);
        data3.moveToFirst();
        Lat = data3.getDouble(data3.getColumnIndex("_latcoord"));
        return Lat;
    }

    public Cursor MessagingCursor(){
        Log.d(TAG," Cursor initialised");
            SQLiteDatabase db = getWritableDatabase();
            String query = "SELECT " + COLUMN_DEVICENAME+ ", "+COLUMN_DEVICENUMBER+ ", "+COLUMN_DEVICEPASSWORD +" FROM " + TABLE_DEVICES +
                    " WHERE "+ COLUMN_TRACKING +" ='1'";

            Cursor c = db.rawQuery(query, null);
        Log.d(TAG," Sending Cursor");
        return c;
    }
    public Cursor MapCursor(){
        SQLiteDatabase db = getWritableDatabase();
        String query = "SELECT " + COLUMN_DEVICENAME+ ", "+COLUMN_DEVICENUMBER+", "+COLUMN_LATCOORD+", "+COLUMN_LONGCOORD+ " FROM " + TABLE_DEVICES +
                " WHERE "+ COLUMN_TRACKING +" ='1' AND " + COLUMN_LATCOORD+" IS NOT NULL AND "+COLUMN_LONGCOORD+" IS NOT NULL";
        Cursor MapCursor = db.rawQuery(query, null);
        return MapCursor;
    }
    public void notifyChange(Context context){
//context = super.
        Intent dbIntent = new Intent("NEW");
        dbIntent.setAction("NEW");
        context.sendBroadcast(dbIntent);
    }

    }


