package com.example.dante.GPSApplication;



import android.content.Intent;
import android.database.Cursor;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class DeviceList extends AppCompatActivity {
    private static DeviceList insts;

    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;

    public static DeviceList instance() {
        return insts;
    }

    @Override
    public void onStart() {
        super.onStart();
        insts = this;
    }


    /**
     * Created by User on 2/28/2017.
     */

        private static final String TAG = "ListDataActivity";

        MyDBHandler newDBHandler;

        public ListView mListView;


        @Override
        protected void onCreate(Bundle savedInstanceState) {
            Log.d(TAG, "STARTING");
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_device_list);
           mListView = (ListView) findViewById(R.id.listView);
           newDBHandler = new MyDBHandler(this, null, null, 1);
            Log.d(TAG, "populateListView: Displaying data in the ListView.");
            populateListView();
        }


        private void populateListView() {
            Log.d(TAG, "populateListView: Displaying data in the ListView.");

            //get the data and append to a list
            Cursor data = newDBHandler.getData();
            ArrayList<String> listData = new ArrayList<>();
            while(data.moveToNext()){
                //get the value from the database in column 1
                //then add it to the ArrayList
                String str = "";
                str +=data.getString(1)+"\n" +data.getString(2);
                listData.add(data.getString(1));
               // listData.add(str);
            }
            //create the list adapter and set the adapter
            ListAdapter adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, listData);
            mListView.setAdapter(adapter);

            //set an onItemClickListener to the ListView
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String name = adapterView.getItemAtPosition(i).toString();
                    //requesting device's password and number, according to their name

                    //String password = newDBHandler.getItemPassword(name).toString();
                    //String number = newDBHandler.getItemNumber(name).toString();


                    Log.d(TAG, "onItemClick: You Clicked on " + name);

                    Cursor data = newDBHandler.getItemID(name);
//                    Cursor data2 = newDBHandler.getItemNumber(name);
                    //Cursor data3 = newDBHandler.getDevice(name);//get the id associated with that name
                    int itemID = -1;
                    String itemNumber="empty";
                    String itemPassword="empty";
                    double lat =0;
                    double lng =0;
                    int isconfirmed=0;
                    int tracking=0;
                    while(data.moveToNext()){
                        itemID = data.getInt(0);
                       itemPassword=data.getString(3);
                        itemNumber=data.getString(2);

                        lat=data.getDouble(data.getColumnIndex("_latcoord"));
                        Log.d(TAG, "VALUE OF lat IS "+lat);

                        lng=data.getDouble(data.getColumnIndex("_longcoord"));
                        Log.d(TAG, "VALUE OF long IS "+lng);

                        isconfirmed=data.getInt(data.getColumnIndex("_confirmed"));

                        Log.d(TAG, "VALUE OF CONFIRMATION IS "+isconfirmed);

                        tracking=data.getInt(data.getColumnIndex("_tracking"));
                        Log.d(TAG, "VALUE OF CONFIRMATION IS "+tracking);

                        //toastMessage(itemPassword);
//                        itemNumber=data3.getString(2);

                    }
                    if(itemID > -1){
                        Log.d(TAG, "onItemClick: The ID is: " + itemID);
                        Intent editScreenIntent = new Intent(DeviceList.this, EditDevice.class);
                        editScreenIntent.putExtra("id",itemID);
                        editScreenIntent.putExtra("name",name);
                        editScreenIntent.putExtra("password", itemPassword);
                        editScreenIntent.putExtra("number", itemNumber);

                        editScreenIntent.putExtra("lat", lat);
                        editScreenIntent.putExtra("lng", lng);
                        editScreenIntent.putExtra("isconfirmed", isconfirmed);
                        editScreenIntent.putExtra("tracking", tracking);
                        Log.d(TAG, "start plox ");
                        startActivity(editScreenIntent);
                    }
                    else{
                        toastMessage("No ID associated with that name");
                    }
                }
            });
        }

    @Override
    protected void onRestart() {
        recreate();
        //super.recreate();
        super.onRestart();
    }

    /**
        // * customizable toast
         //* @param message
        // */


        private void toastMessage(String message){
            Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
        }
    }


