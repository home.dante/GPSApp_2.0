package com.example.dante.GPSApplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ToggleButton;


public class MainActivity extends AppCompatActivity {

String TAG = "TEST";
    MyDBHandler mDatabaseHelper;
    public Button trackthis, test;
    ToggleButton track;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        mDatabaseHelper = new MyDBHandler(this,"devices", null, 1);
        mDatabaseHelper.cleanup();
        trackthis= (Button) findViewById(R.id.btnTrackThis);

        test=(Button) findViewById(R.id.btnTest);
       // track=(ToggleButton) findViewById(R.id.track);
        trackthis.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            Intent intent = new Intent(MainActivity.this, TrackThis.class);
                startActivity(intent);
            }
            });
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               MyGlobalVariable appState = ((MyGlobalVariable) getApplicationContext());

                Intent intent = new Intent (MainActivity.this, HandlerWrapper.class);
                intent.putExtra("msg", "t005s002123456");
                intent.putExtra("addr", "1");

               startService(intent);

            }
        });
//        track.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    // The toggle is enabled
//                    MyGlobalVariable appState = ((MyGlobalVariable) getApplicationContext());
//                    appState.setGlobaltrue();
//
//                } else {
//                    MyGlobalVariable appState = ((MyGlobalVariable) getApplicationContext());
//                    appState.setGlobalfalse();
//                    stopService(new Intent(MainActivity.this, MyDBHandler.class));
//                }
//            }
//        });
    }
   public void OpenMessages(View view) {
       Intent intent = new Intent(this, SmsCatch.class);

        startActivity(intent);
    }
    public void openmaps(View view) {
        Intent intent = new Intent(this, MapsActivity.class);

        startActivity(intent);
    }
    public void opendevices(View view){
        Intent devintent = new Intent(this, AddDevice.class);
        startActivity(devintent);
    }
    public void ViewList(View view){
        Log.d(TAG, "button press.");
        Intent devintent = new Intent(MainActivity.this, DeviceList.class);
        Log.d(TAG, "button press, start.");
        startActivity(devintent);
    }




    public void TrackAll(View view){
        Log.d(TAG, "button press.");
        Intent devintent = new Intent(MainActivity.this, GetCoords.class);
        Log.d(TAG, "button press, start.");
        startActivity(devintent);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
