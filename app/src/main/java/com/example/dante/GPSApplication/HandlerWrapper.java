package com.example.dante.GPSApplication;

import android.Manifest;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.SmsManager;
import android.util.Log;
import android.app.IntentService;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;
import android.widget.CompoundButton;

import com.google.android.gms.tasks.Task;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.location.LocationManager.NETWORK_PROVIDER;
import static android.location.LocationManager.GPS_PROVIDER;

public class HandlerWrapper extends IntentService {
    private LocationManager mLocationManager = null;
    private static final int LOCATION_INTERVAL = 1000;
    private static final float LOCATION_DISTANCE = 0;
    private LocationManager locMan;
    private LocationListener locList;
    Boolean status = false;
    double lat;
    double lng;
    // LocationProvider getnew = new LocationProvider()
    String password = "123456";
    private String timeUnit, coordString;
    private int timeCount, messageCount;
    int count = 0;
    String msg1 = "";
    String addr1 = "";
    SmsManager smsManager = SmsManager.getDefault();

    private static final String TAG = "SERVICE ";

    public HandlerWrapper() {
        super("HandlerWrapper");
    }

    private class LocationListener implements android.location.LocationListener {
        Location mLastLocation;

        public LocationListener(String provider) {
            Log.e(TAG, "LocationListener " + provider);
            mLastLocation = new Location(provider);
        }

        @Override
        public void onLocationChanged(Location location) {
            Log.e(TAG, "onLocationChanged: " + location);
            mLastLocation.set(location);
            lng = mLastLocation.getLongitude();
            lat = mLastLocation.getLatitude();
        }

        @Override
        public void onProviderDisabled(String provider) {
            Log.e(TAG, "onProviderDisabled: " + provider);
        }

        @Override
        public void onProviderEnabled(String provider) {
            Log.e(TAG, "onProviderEnabled: " + provider);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {
            Log.e(TAG, "onStatusChanged: " + provider);
        }
    }

    LocationListener[] mLocationListeners = new LocationListener[]{
            new LocationListener(LocationManager.GPS_PROVIDER),
            new LocationListener(LocationManager.NETWORK_PROVIDER)
    };


    //MAIN FUNCTIONALITY IS HERE
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        return super.onStartCommand(intent, flags, startId);
        msg1 = intent.getStringExtra("msg");
        addr1 = intent.getStringExtra("addr");
        status = intent.getBooleanExtra("status", false);
        //testing bit
//        smsManager.sendTextMessage(addr1, null, msg1+" onhandle", null, null);
//        parseMsg(msg1);
//        setCoordString();
//        doToast();
        //testing bit ends

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.

        }
        //mLocationManager.getLastKnownLocation(GPS_PROVIDER);


                Log.d(TAG, "MSG IS " + msg1);
                if (msg1.contains("t0")) {
                    Log.d(TAG, "contains t0");
                }
                if (msg1.contains(password)) {
                    Log.d(TAG, "contains " + password);
                }

                if (msg1.contains("SMSTEXT" + password)) {
                    Log.d(TAG, "RECEIVED");
                    // smsManager.sendTextMessage(smsAddress, null, "current coords", null, null);
                    //toastMessage("sending current coords");
setCoordString();
                        //doWait();
                   //doWait();
                    smsManager.sendTextMessage(addr1, null, coordString, null, null);
                    stopSelf();


                } else if (msg1.contains("t0") && msg1.contains(password)) {
                    // toastMessage("sending multiple coords");
                    parseMsg(msg1);
                    setCoordString();
                    doToast();
                    stopSelf();
                }
//            else
//            if (msg1.contains("begin" + password)) {
//                // Log.d(TAG, "REACHED IF STATEMENT");
//                // Log.d(TAG, "begin" + password + " number is" + edit.getText().toString());
//               // smsManager.sendTextMessage(addr1, null, "begin ok", null, null);
//
//                stopSelf();
//
//            }
//            else if (msg1.contains("begin")) {
//                smsManager.sendTextMessage(addr1, null, "fail", null, null);
//                stopSelf();
//            }
                else {
                    stopSelf();
                }


            Log.d(TAG, msg1);
            Log.d(TAG, addr1);
            return START_STICKY;
        }

//MAIN FUNCTIONALITY ENDS





        @Override
        public void onCreate() {

            Log.e(TAG, "onCreate");
            initializeLocationManager();
            try {
                mLocationManager.requestLocationUpdates(
                        LocationManager.NETWORK_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                        mLocationListeners[1]);
            } catch (java.lang.SecurityException ex) {
                Log.i(TAG, "fail to request location update, ignore", ex);
            } catch (IllegalArgumentException ex) {
                Log.d(TAG, "network provider does not exist, " + ex.getMessage());
            }
            try {
                mLocationManager.requestLocationUpdates(
                        LocationManager.GPS_PROVIDER, LOCATION_INTERVAL, LOCATION_DISTANCE,
                        mLocationListeners[0]);
            } catch (java.lang.SecurityException ex) {
                Log.i(TAG, "fail to request location update, ignore", ex);
            } catch (IllegalArgumentException ex) {
                Log.d(TAG, "gps provider does not exist " + ex.getMessage());
            }


            super.onCreate();
//        stopSelf();
        }

        @Override
        public IBinder onBind(Intent intent) {
            // TODO: Return the communication channel to the service.
            return null;
            //throw new UnsupportedOperationException("Not yet implemented");
        }

        @Override
        public void onDestroy() {

            super.onDestroy();
        }

        @Override
        protected void onHandleIntent(@Nullable Intent intent) {

            Log.e(TAG, "onDestroy");
            super.onDestroy();
            if (mLocationManager != null) {
                for (int i = 0; i < mLocationListeners.length; i++) {
                    try {
                        mLocationManager.removeUpdates(mLocationListeners[i]);
                    } catch (Exception ex) {
                        Log.i(TAG, "fail to remove location listners, ignore", ex);
                    }
                }
            }

            Log.d(TAG,"SimpleWakefulReceiver"+ "Completed service @ " + SystemClock.elapsedRealtime());
            SmsBroadcastReceiver.completeWakefulIntent(intent);
        }
        public void parseMsg(String s){



            //  String s = new String("t029h006123456");
            Matcher matcher = Pattern.compile("t\\d\\d\\d").matcher(s);
            Matcher matcher2 = Pattern.compile("(h|m|s)\\d\\d\\d").matcher(s);
            matcher.find();
            matcher2.find();
            //int i = Integer.valueOf(matcher.group());
            String random= String.valueOf(matcher.group());
            String random2= String.valueOf(matcher2.group());
            //System.out.println("interval time is " +Integer.valueOf(random.substring(1))+" "+ random2.substring(0,1) +" units \n");
            //System.out.println(random.substring(1)+"\n");

            //System.out.println("number of messages is "+Integer.valueOf(random2.substring(1)));
            timeUnit= random2.substring(0,1);
            switch(timeUnit) {
                case "s":
                    timeCount= 1000*Integer.valueOf(random.substring(1));;
                    break;
                case "m":
                    timeCount= 60*1000*Integer.valueOf(random.substring(1));;
                    break;
                case "h":
                    timeCount= 60*60*1000*Integer.valueOf(random.substring(1));
                default:
                    timeCount= 1000*Integer.valueOf(random.substring(1));
            }


            messageCount = Integer.valueOf(random2.substring(1));




        }
        public void doToast () {

            final Handler handler= new Handler();

//TODO MAKE TOGGLE BUTTON CHECK INSTEAD OF TRUE VALUE

            handler.postDelayed(new Runnable(){

                @Override
                public void run() {if (count<messageCount && true) {
                    // TODO Auto-generated method stub

                    handler.postDelayed(this, timeCount);

                    setCoordString();
                    smsManager.sendTextMessage(addr1, null, coordString, null, null);
Log.d(TAG,coordString);

                    count++;
                }
                }

            }, 1000);
            count=0;

        }


        public void setCoordString() {
            coordString="lat:"+lat+" "+"long:"+lng+" "+"speed:0";

        }



    private void initializeLocationManager() {
        Log.e(TAG, "initializeLocationManager");
        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getApplicationContext().getSystemService(getApplicationContext().LOCATION_SERVICE);
        }










    }











    public void doWait () {

        final Handler handler= new Handler();

//TODO MAKE TOGGLE BUTTON CHECK INSTEAD OF TRUE VALUE

        handler.postDelayed(new Runnable(){

            @Override
            public void run() {while(lat==0) {
                // TODO Auto-generated method stub

                handler.postDelayed(this, 1000);

                setCoordString();
                if(lat!=0) smsManager.sendTextMessage(addr1, null, coordString, null, null);
                //Log.d(TAG,coordString);

            }
            }

        }, 1000);

//        setCoordString();
//        smsManager.sendTextMessage(addr1, null, coordString, null, null);
//        Log.d(TAG,coordString);
    }

    }


