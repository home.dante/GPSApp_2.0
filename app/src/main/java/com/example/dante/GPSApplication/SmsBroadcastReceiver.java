package com.example.dante.GPSApplication;




import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import android.os.Handler;
import android.support.v4.content.WakefulBroadcastReceiver;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;

import static android.R.id.message;



public class SmsBroadcastReceiver extends WakefulBroadcastReceiver {
    SmsManager smsManager = SmsManager.getDefault();
    private LocationManager locMan;
    private LocationListener locList;
int count =0;
    MyDBHandler newdbhandler;
    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
        private static final String TAG="DEBUG";


        public static final String SMS_BUNDLE = "pdus";
String password ="123456";
    Boolean status=false;
    public static final String ACTION_UNREAD_SMS = "com.example.dante.GPSApplication.ACTION_UNREAD_SMS";
    public static final String EXTRA_SMS_BODY = "com.example.dante.GPSApplication.EXTRA_SMS_BODY";
    public static final String EXTRA_SMS_ADDRESS = "com.example.dante.GPSApplication.EXTRA_SMS_ADDRESS";
public static final String DATABASE_CHANGED="com.example.dante.GPSApplication.DATABASE_CHANGED";
    public static final String PRE_ADDRESS = "03590000004";



    @Override
        public void onReceive(Context context, Intent intent) {


        newdbhandler = new MyDBHandler(context, null, null, 1);

            Bundle intentExtras = intent.getExtras();

            if (intentExtras != null) {
                Object[] sms = (Object[]) intentExtras.get(SMS_BUNDLE);
                String smsMessageStr = "";
                String addr ="";
                String msg="";
              // Log.d(TAG, "SMS:00000000000000000000000000000000000 " +sms.length );

                    for (int i = 0; i < sms.length; ++i) {


                       // Log.d(TAG, "SMS:00000000000000000000000000000000000 " +sms.length );
                        String format = intentExtras.getString("format");
                        //SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i], format);

                        SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) sms[i]);

                        String smsBody = smsMessage.getMessageBody().toString();
                        String address = smsMessage.getOriginatingAddress();


Log.d(TAG, "MESSAGE NUMBER IS "+ address);
                        addr=address;
                        msg=smsBody;
                        smsMessageStr += "SMS From: " + address + "\n";
                        smsMessageStr += smsBody + "\n";

                        //TODO:Check if the number is in a database
                        Boolean check = newdbhandler.CheckForExistance(address);

                        if (check) {

                            Log.d(TAG, " TRUE");
                            //TODO IS TRACKING ENABLED?
                            Cursor newcursor =newdbhandler.isTracking(address);
                            int checkfortracking=0;
                            Log.d(TAG, " INT "+checkfortracking);
                            newcursor.moveToFirst();

                                Log.d(TAG, " while" );
                                checkfortracking = newcursor.getInt(newcursor.getColumnIndex("_tracking"));
                                Log.d(TAG, " INT "+checkfortracking);




                            if ( checkfortracking ==1) {
                                Log.d(TAG, "TRACKING IS ENABLED");
                                //TODO: Check if it is a coord or not
                                Log.d(TAG, msg);
                                if (msg.matches("lat:(-|)\\d+\\.\\d+ long:(-|)\\d+\\.\\d+ .+")) {
                                    //TODO:If it is a coord
                                    Log.d(TAG, "is a coord");
//                                //TODO: EXTRACT COORDINATES and put them into the database
                                    parsecoords(context,smsBody, address);
                                    Log.d(TAG, "PARSED");

                                } else {

                                    //TODO is it a response to pairing request?
                                    if (smsMessageStr.contains("begin ok")) {
                                        //TODO is response positive?
                                        // if(smsMessageStr.equals("ok"))
                                        Log.d(TAG, "confirm device");
                                        newdbhandler.ConfirmDevice(address);
                                    } else if (smsMessageStr.equals("begin fail")) {
                                        //TODO is it negative?
                                        //inform the user
                                        //display authentification failed in the device window

                                    }
                                }
                            }
                            else{
                                Log.d(TAG, "tracking not enabled");
                            }
                        }
                        //TODO NOT IN DATABASE
                        else if(smsMessageStr.equals("begin"+password)){
//                            Confirmed.add(address);
//                            smsManager.sendTextMessage(address, null, "begin ok", null, null);

                        }
                        else{
                            Log.d(TAG,"FALSE");
                            //smsManager.sendTextMessage(address, null, "fail", null, null);

                            //just ignore the message
                        }

                    }




                Intent smsIntent = new Intent(ACTION_UNREAD_SMS);
                smsIntent.putExtra(EXTRA_SMS_BODY, msg);
                smsIntent.putExtra(EXTRA_SMS_ADDRESS, addr);
                context.sendBroadcast(smsIntent);

                if(msg.equals("begin"+password)) {
                    newdbhandler.addAuthorized(addr);
                    smsManager.sendTextMessage(addr, null, "begin ok", null, null);
                    Log.d(TAG, "confirmed "+addr);
                }


                Log.d(TAG,"CONTAINS "+addr);
                Intent Sintent = new Intent(context, HandlerWrapper.class);
                Sintent.putExtra("msg", msg);
                Sintent.putExtra("addr", addr);

                Sintent.putExtra("status", status);
                Sintent.setAction(ACTION_UNREAD_SMS);
              //MyGlobalVariable appState = ((MyGlobalVariable)context);

                if(newdbhandler.CheckIfAuthorized(addr)) {
                    Log.d(TAG, "TRUE");
                    startWakefulService(context, Sintent);
                    Log.d(TAG,"END OF SERVICE");
                }


            }
        }
        //TODO filter methods
    //Check if message is containing coordonates
    public Boolean CheckIfCoord(String message){
        if (message.contains("lat:") && message.contains("long:")){
            return true;
        }
        else {

            return false;
        }

    }
        public void parsecoords (Context context, String message, String addr){
            //EXTRACTS COORDS FROM MESSAGE
            double lng;
            double lat;

        String  messagelat = new String(message);
        String messagelong = new String(message);


        messagelat = messagelat.substring(messagelat.indexOf(":") + 1);
        messagelat = messagelat.substring(0, messagelat.indexOf(" "));
        lat = Double.valueOf(messagelat);

///comentariu
        messagelong = messagelong.substring(messagelong.indexOf(':', messagelong.indexOf(':') + 1));
        messagelong = messagelong.substring(0, messagelong.indexOf(" "));
        lng = Double.valueOf(messagelong.substring(1));
            newdbhandler.SetLat(addr, lat);
            newdbhandler.SetLong(addr, lng);

            Intent dbIntent= new Intent();
            dbIntent.setAction(DATABASE_CHANGED);
            //dbIntent.putExtra("Changed", 1);
            context.sendBroadcast(dbIntent);



    }

//TODO END OF FILTER methods
    }

