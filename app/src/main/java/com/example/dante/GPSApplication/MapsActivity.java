package com.example.dante.GPSApplication;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ZoomControls;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;

import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Vector;

import static android.R.attr.action;
import static com.example.dante.GPSApplication.SmsBroadcastReceiver.DATABASE_CHANGED;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {
    private void toastMessage(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }
    private static final String TAG = "Maps";
    MyDBHandler newdbhandler;
    private GoogleMap mMap;
    ZoomControls  zoom;
    Button refresh, btnGo;
    EditText textGo;
    Button btnIteratePositive, btnIterateNegative;
    ArrayList<Double> longlist= new ArrayList<Double>();
    ArrayList<Double> latlist= new ArrayList<Double>();


    public int counter=0;

   // public static final String DATABASE_CHANGED = " tabledevices.DATABASE_CHANGED";
    private BroadcastReceiver mUnreadSmsBroadCastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {

            //String smsBody = intent.getStringExtra(SmsBroadcastReceiver.EXTRA_SMS_BODY);
            //  String action = intent.getAction(DATABASE_CHANGED);
           // String changed = intent.getStringExtra("Changed");
         //   Log.d(TAG, action);
          //  if (action.equals("DATABASE_CHANGED")){

                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);
                recreate();
            //}

        }
    };
//static public void reboot()
//{
//    finish();
//    overridePendingTransition(0, 0);
//    startActivity(getIntent());
//    overridePendingTransition(0, 0);
//    recreate();
//}

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mUnreadSmsBroadCastReceiver);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        newdbhandler = new MyDBHandler(this, null, null, 1);
        btnGo= (Button) findViewById(R.id.btnGo);
        btnIterateNegative= (Button) findViewById(R.id.btnIterateNegative);
        btnIteratePositive= (Button) findViewById(R.id.btnIteratePositive);
        textGo =(EditText) findViewById(R.id.textGo);
       // textGo.setText(""+counter);
        refresh=(Button) findViewById(R.id.refresh);
        zoom = (ZoomControls ) findViewById(R.id.zcZoom);
        zoom.setOnZoomOutClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.zoomOut());
            }
        });
        zoom.setOnZoomInClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mMap.animateCamera(CameraUpdateFactory.zoomIn());
            }
        });
        refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(0, 0);
                startActivity(getIntent());
                overridePendingTransition(0, 0);

                recreate();

            }
        });
        btnGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                double lat=0;
                double lng=0;
                if(newdbhandler.CheckForName(String.valueOf(textGo.getText() )) )
                {
                    lat = newdbhandler.GetLat(String.valueOf(textGo.getText()));
                    lng = newdbhandler.GetLong(String.valueOf(textGo.getText()));
                    LatLng ll = new LatLng(lat, lng);
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
                    mMap.moveCamera(update);
                }
                else{
                    toastMessage("Device doesn't exist");
                }
            }
        });
        btnIteratePositive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(counter<latlist.size()-1 && counter<latlist.size()-1 &&latlist.size()!=0)
                {

                    counter++;
                    //toastMessage("counter incremented: " + counter);
                    //textGo.setText(""+counter);



                  //  LatLng ll = new LatLng(vlat[counter], vlong[counter]);
                    LatLng ll = new LatLng(latlist.get(counter), longlist.get(counter));
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
                    mMap.moveCamera(update);




                }
                else if(latlist.size()!=0){
                    toastMessage(">");
                    LatLng ll = new LatLng(latlist.get(counter), longlist.get(counter));
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
                    mMap.moveCamera(update);
                }


            }
        });
        btnIterateNegative.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(counter>0 &&latlist.size()!=0)
                {


                        counter--;
                        // toastMessage("counter decremented: "+counter);
                        //textGo.setText(""+counter);


                    LatLng ll = new LatLng(latlist.get(counter), longlist.get(counter));
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
                    mMap.moveCamera(update);


                }
                else if(latlist.size()!=0){
                    toastMessage("<");
                    LatLng ll = new LatLng(latlist.get(counter), longlist.get(counter));
                    CameraUpdate update = CameraUpdateFactory.newLatLngZoom(ll, 15);
                    mMap.moveCamera(update);
                }


            }
        });

    }

    private void DisplayCoords(){
        counter=0;
        latlist.clear();
        longlist.clear();
        Cursor MainCursor = newdbhandler.MapCursor();
        MainCursor.moveToFirst();
        while (!MainCursor.isAfterLast()) {
            if (MainCursor.getString(MainCursor.getColumnIndex("_devicename")) != null) {
                LatLng ll = new LatLng(Double.valueOf( MainCursor.getString(MainCursor.getColumnIndex("_latcoord")) ), Double.valueOf( MainCursor.getString(MainCursor.getColumnIndex("_longcoord")) ));
                CameraUpdate update  =  CameraUpdateFactory.newLatLngZoom(ll,15);
                mMap.addMarker(new MarkerOptions().position(ll).icon(BitmapDescriptorFactory
                        .defaultMarker(BitmapDescriptorFactory.HUE_AZURE))
                        .title(MainCursor.getString(MainCursor.getColumnIndex("_devicename"))));
                      // v.addElement(new double(1) );
                Log.d(TAG,"LAT: "+MainCursor.getString(MainCursor.getColumnIndex("_latcoord")));
                Log.d(TAG,"LONG: "+MainCursor.getString(MainCursor.getColumnIndex("_longcoord")));

               // vlat[size]=(Double.valueOf( MainCursor.getString(MainCursor.getColumnIndex("_latcoord"))));
               // vlong[size]=(Double.valueOf( MainCursor.getString(MainCursor.getColumnIndex("_longcoord"))));
                latlist.add((Double.valueOf( MainCursor.getString(MainCursor.getColumnIndex("_latcoord")))));
                longlist.add((Double.valueOf( MainCursor.getString(MainCursor.getColumnIndex("_longcoord")))));
               // size++;
                mMap.moveCamera(update);

            }
//

            MainCursor.moveToNext();
        }
        toastMessage("Map updated");
    }

    @Override
    public void onResume(){
        super.onResume();
        registerReceiver(mUnreadSmsBroadCastReceiver, new IntentFilter(SmsBroadcastReceiver.DATABASE_CHANGED));
        if(mMap != null){ //prevent crashing if the map doesn't exist yet (eg. on starting activity)
            mMap.clear();

            // add markers from database to the map
        }
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        //newdbhandler.onOpen(recreate());
        mMap = googleMap;
        DisplayCoords();
        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
