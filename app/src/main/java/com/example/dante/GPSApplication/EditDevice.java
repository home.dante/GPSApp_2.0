package com.example.dante.GPSApplication;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class EditDevice extends AppCompatActivity {

    private static final int READ_SMS_PERMISSIONS_REQUEST = 1;
    SmsManager smsManager = SmsManager.getDefault();
        public static final String TAG = "EditDataActivity";

        private Button btnSave,btnDelete, btnGetCoords,btnResend;
        private EditText editable_item, edit_number, edit_password;
        private TextView confirmText, latText, lngText;
        private CheckBox tracking;
        MyDBHandler mDatabaseHelper;
        Boolean checked = false;
        private String selectedName, selectedPassword, selectedNumber;
    private int  selectedIsConfirmed, selectedTracking;
    private double selectedLng=0, selectedLat=0;
        private int selectedID;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            Log.d(TAG, "onCreate");
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_edit_device);
            btnSave = (Button) findViewById(R.id.btnSave);
            btnDelete = (Button) findViewById(R.id.btnDelete);

            editable_item = (EditText) findViewById(R.id.editname);
            edit_number =(EditText) findViewById(R.id.editnumber);
            edit_password=(EditText) findViewById(R.id.editpassword);
            confirmText=(TextView) findViewById(R.id.confirmstatus);
            latText=(TextView) findViewById(R.id.latText);
            lngText=(TextView) findViewById(R.id.lngText);
            tracking=(CheckBox) findViewById(R.id.checkTracking);
            btnGetCoords=(Button) findViewById(R.id.GetCoords);
            btnResend=(Button) findViewById(R.id.Resend);

            mDatabaseHelper = new MyDBHandler(this, null, null, 1);

            //get the intent extra from the ListDataActivity
            Intent receivedIntent = getIntent();

            //now get the itemID we passed as an extra
            selectedID = receivedIntent.getIntExtra("id",-1); //NOTE: -1 is just the default value

            //now get the name we passed as an extra
            selectedName = receivedIntent.getStringExtra("name");
            selectedPassword = receivedIntent.getStringExtra("password");
           selectedNumber = receivedIntent.getStringExtra("number");
            selectedLat = receivedIntent.getDoubleExtra("lat",0);
            selectedLng=receivedIntent.getDoubleExtra("lng",0);
           selectedIsConfirmed=receivedIntent.getIntExtra("isconfirmed",0);
            selectedTracking=receivedIntent.getIntExtra("tracking",0);

            //set the text to show the current selected name
            editable_item.setText(selectedName);
            edit_number.setText(selectedNumber);
            edit_password.setText(selectedPassword);


            latText.setText("Latitude: "+String.valueOf(selectedLat) );
            lngText.setText("Longitude: "+String.valueOf(selectedLng) ) ;

            if (selectedIsConfirmed==0) {
                confirmText.setText("NOT CONFIRMED");
                confirmText.setTextColor(Color.RED);
            }
            else
            {
                confirmText.setText("CONFIRMED");
                confirmText.setTextColor(Color.GREEN);
            }




            if(selectedTracking==0){
                tracking.setChecked(false);
            }
            else if(selectedTracking==1)
                {
                tracking.setChecked(true);
            }
            else
            {
                tracking.setChecked(false);
                toastMessage("Tracking status not available");
            }
            tracking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    // TODO Auto-generated method stub
                    checked = true;

                }
            });

                    btnSave.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String item = editable_item.getText().toString();
                    String number = edit_number.getText().toString();
                    String password = edit_password.getText().toString();

Log.d(TAG, "before if");
                    if(!item.equals("") ){
                        //mDatabaseHelper.updateName(item,selectedID,selectedName);
                        if(!number.equals("")){
                            if(!password.equals("")){
                                if(!item.equals(selectedName) || !number.equals(selectedNumber)|| !password.equals(selectedPassword) ||checked ) {
                                    mDatabaseHelper.updateName(item, selectedID, selectedName);
                                    mDatabaseHelper.updatePassword(password, selectedID, selectedPassword);
                                    mDatabaseHelper.updateNumber(number, selectedID, selectedNumber);
                                    if(tracking.isChecked()){
                                        mDatabaseHelper.SetTracking(number, 1);
                                    }
                                    else{
                                        mDatabaseHelper.SetTracking(number, 0);
                                    }
                                    Log.d(TAG, "PASSING VARIABLES PASSWORD AND NUMBER: " + password + "  " + number);
                                    Log.d(TAG, "PASSING VARIABLES old PASSWORD AND old NUMBER: " + selectedPassword + "  " + selectedNumber);
                                    //NOTE!Implement checking if nothing was changed(selecteditem==item)
                                    //NOTE! implement checking if device with the same name/number exists
                                    toastMessage("Device has been succesfully edited");
                                }
                                else{
                                    toastMessage("No attribute was changed");
                                }
                            }
                            else{
                                toastMessage("You must enter a password");
                            }
                        }
                        else{
                            toastMessage("You must enter a number");
                        }
                    }else{
                        toastMessage("You must enter a name");
                    }
                }
            });

            btnDelete.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mDatabaseHelper.deleteName(selectedID,selectedName);
                    editable_item.setText("");
                    edit_number.setText("");
                    edit_password.setText("");
                    toastMessage("Device has been successfully removed");
                    //super.onBackPressed();
                    end();
                }
            });
            btnResend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ContextCompat.checkSelfPermission(EditDevice.this, Manifest.permission.SEND_SMS)
                            != PackageManager.PERMISSION_GRANTED) {
                        getPermissionToReadSMS();
                    } else {

                        smsManager.sendTextMessage(edit_number.getText().toString(), null, "begin"+edit_password.getText().toString(), null, null);
                    }
                }
            });
            btnGetCoords.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ContextCompat.checkSelfPermission(EditDevice.this, Manifest.permission.SEND_SMS)
                            != PackageManager.PERMISSION_GRANTED) {
                        getPermissionToReadSMS();
                    } else {

                        smsManager.sendTextMessage(edit_number.getText().toString(), null, "SMSTEXT"+edit_password.getText().toString(), null, null);
                    }
                }
            });



        }
        public void end() {
            super.onBackPressed();
        }
//         btnGetCoords.setOnClickListener(new View.OnClickListener(){
//        @Override
//        public void onClick(View view) {
//            if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
//                    != PackageManager.PERMISSION_GRANTED) {
//                getPermissionToReadSMS();
//            } else {
//                smsManager.sendTextMessage(edit_number.getText().toString(), null, "SMSTEXT"+edit_password.getText().toString(), null, null);
//                //Toast.makeText(this, "Message sent!", Toast.LENGTH_SHORT).show();
//            }
//
//
//        }
//
//
//    });
public void GetCoordsOnClick(View view) {
    if (ContextCompat.checkSelfPermission(this, Manifest.permission.SEND_SMS)
            != PackageManager.PERMISSION_GRANTED) {
        getPermissionToReadSMS();
    } else {
        smsManager.sendTextMessage(edit_number.getText().toString(), null, "SMSTEXT"+edit_password.getText().toString(), null, null);
        //Toast.makeText(this, "Message sent!", Toast.LENGTH_SHORT).show();
    }


}
    public void getPermissionToReadSMS() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_SMS)
                    != PackageManager.PERMISSION_GRANTED) {

                if (shouldShowRequestPermissionRationale(
                        Manifest.permission.READ_SMS)) {
                    Toast.makeText(this, "Please allow permission!", Toast.LENGTH_SHORT).show();
                }

                requestPermissions(new String[]{Manifest.permission.READ_SMS},
                        READ_SMS_PERMISSIONS_REQUEST);
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        // Make sure it's our original READ_CONTACTS request
        if (requestCode == READ_SMS_PERMISSIONS_REQUEST) {
            if (grantResults.length == 1 &&
                    grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(this, "Read SMS permission granted", Toast.LENGTH_SHORT).show();

            } else {
                Toast.makeText(this, "Read SMS permission denied", Toast.LENGTH_SHORT).show();
            }

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }



    }

        /**
         * customizable toast
         * @param message
         */
        private void toastMessage(String message){
            Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
        }
//        private String IntToString(int val){
//
//            if (val==0){
//                return "false";
//            }
//            else return "true";
//        }
    }

